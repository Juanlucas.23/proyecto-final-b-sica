function mapa(){

    let mapa = L.map('mapa',{center: [4.751152, -74.062952],zoom: 16});
    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',{
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    
    mosaico.addTo(mapa); 

    let barrio = L.polygon([
        [4.755510558967133,-74.0657714009285],
        [4.747892536397094,-74.06874060630798],
        [4.747443471386982,-74.06726002693176],
        [4.747507623549183,-74.06668066978453],
        [4.746823333511531,-74.06128406524658],
        [4.746620184775969,-74.05976593494415],
        [4.753885387884816,-74.05812442302704],
        [4.755510558967133,-74.0657714009285],
    ]).addTo(mapa);

    let marcador1 = L.marker([4.7502862083486805,-74.06542807817459]);
    marcador1.addTo(mapa);

    let marcador2 = L.marker([4.748416445206086,-74.0624213218689]);
    marcador2.addTo(mapa);

    let marcador3 = L.marker([4.747518315575633,-74.06380265951157]);
    marcador3.addTo(mapa);

    let marcador4 = L.marker([4.750072368646301,-74.06240992248058]);
    marcador4.addTo(mapa);

    let marcador5 = L.marker([4.752065084801941,-74.0629631280899]);
    marcador5.addTo(mapa);
}
