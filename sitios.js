function mapa(){

    let mapa = L.map('mapa',{center: [4.751152, -74.062952],zoom: 16});
    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',{
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    
    mosaico.addTo(mapa); 

    let barrio = L.polygon([
        [4.755510558967133,-74.0657714009285],
        [4.747892536397094,-74.06874060630798],
        [4.747443471386982,-74.06726002693176],
        [4.747507623549183,-74.06668066978453],
        [4.746823333511531,-74.06128406524658],
        [4.746620184775969,-74.05976593494415],
        [4.753885387884816,-74.05812442302704],
        [4.755510558967133,-74.0657714009285],
    ]).addTo(mapa);

    let marcadorClinica = L.marker([4.7502862083486805,-74.06542807817459]);
    marcadorClinica.addTo(mapa).bindPopup("<h1> Clinica la colina </h1><br> Es una institucion privada de salud ubicada en la localidad de suba, Presta sus servicios al publico desde el año 2013. Actualmente hace parte del grupo de la clinica del contry y atiende a las personas del noroccidente de Bogota y los municipios de Chia y Cajica").openPopup();
    
    let marcador2 = L.marker([4.748416445206086,-74.0624213218689]);
    marcador2.addTo(mapa).bindPopup("<h2> Parque Tierracolina </h2><br> Es un parque publico de esparcimiento para la comunidad donde se pueden realizar deportes como el futbol y el baloncesto, tambien cuenta con espacios para que las personas puedan pasear a sus mascotas y los niños puedan divertirse").openPopup();

    let marcador3 = L.marker([4.747518315575633,-74.06380265951157]);
    marcador3.addTo(mapa).bindPopup("<h2> Parque La Chocita </h2><br> Es un parque publico diseñado para el espacimiento de la comunidad. Cuenta con una pequeña cabaña en el centro del parque que le da su nombre caracteristico, a demas de eso cuenta con una cacha mixta donde se puede practicar futbol y baloncesto.").openPopup();

    let marcador4 = L.marker([4.750072368646301,-74.06240992248058]);
    marcador4.addTo(mapa).bindPopup("<h2> Parroquia San Jeronimo Emiliani </h2> Ubicado en la parte central del barrio, es un lugar donde las personas adeptas a la religion catolica puden ir a rendir culto y ejercer su derecho a la libertad de culto. Tienen atencion al publico todos los dias de la semana de 6:30 am a 6:00 pm").openPopup();
    
    let marcador5 = L.marker([4.7499828232512264,-74.05956745147705]);
    marcador5.addTo(mapa).bindPopup("<h2> Centro Comercial Arizona </h2> Ubicado en las limites del barrio es uno de las sitios mas importantes del barrio porque cuenta con gran variedad de establecimientos, ofrece al publico servicios de belleza, entretenimiento, atencion a los usuarios y muchas cosas mas").openPopup();
    
}


